import jinja2
from jinja2 import FileSystemLoader, Environment

class Parrot(object):
    def __init__(self):
        self.condition = True
        self.hello = 'Hello World!'

        self.some_list = [
            {'name': 'Polly'},
            {'name': 'Ara'},
            {'name': 'Papagaaitje'},
        ]

        self.snakes = {
            'a' : 'Anaconda',
            'b' : 'Boa',
            'r' : 'Rattlesnake'
        }

    def say_hello(self):
        return 'I say hello!'

    @property
    def many_thanks(self):
        return 'thanks, ' * 10

def mywrapper(input, width=72, always_break=False):
    return input[0:width] + '\\'   # fixme

if __name__=='__main__':
    env = Environment(
        loader=FileSystemLoader('./templates')
    )
    env.filters['smartwrap'] = mywrapper

    parrot = Parrot()
    template = env.get_template('hello.tmpl')
    text = template.render(myobj=parrot)
    print(text)
